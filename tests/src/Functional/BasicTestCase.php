<?php

namespace Drupal\Tests\dtuber\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests basic functionality of Dtuber.
 *
 * @group dtuber
 */
class BasicTestCase extends BrowserTestBase {

  /**
   * Test user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $account;

  /**
   * {@inheritdoc}
   */
  public $defaultTheme = 'stark';
  /**
   * {@inheritdoc}
   */
  protected static $modules = ['help', 'dtuber'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->account = $this->drupalCreateuser(['access administration pages']);
    $this->drupalLogin($this->account);
  }

  /**
   * Test if help page is shown correctly.
   */
  public function testHelpPage() {
    $this->drupalGet('admin/help/dtuber');
    $this->assertSession()->pageTextContains('Dtuber aka "Drupal YouTuber" - Uploads Video to a YouTube Channel. Provides a new Field type, that can be attached to a Content-type.');
  }

}
