<?php

namespace Drupal\dtuber;

use Drupal\Core\Render\Markup;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * YouTube Service.
 */
class YouTubeService {

  use StringTranslationTrait;

  /**
   * Client id.
   *
   * @var string
   */
  protected $clientId;

  /**
   * Client secret.
   *
   * @var string
   */
  protected $clientSecret;

  /**
   * Redirect uri.
   *
   * @var string
   */
  protected $redirectUri;

  /**
   * Google Client.
   *
   * @var \Google_Client
   */
  protected $client;

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $dtuberLogger;

  /**
   * The state storage service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Returns if all the 3 credentials available.
   */
  public function getCredentials() {
    $clientId = $this->getConfig('client_id');
    $clientSecret = $this->getConfig('client_secret');
    $redirectUri = $this->getConfig('redirect_uri');

    if (isset($clientId) && isset($clientSecret) && isset($redirectUri)) {
      // Credentials present.
      $this->clientId = $clientId;
      $this->clientSecret = $clientSecret;
      $this->redirectUri = $redirectUri;
      return [
        'client_id' => $clientId,
        'client_secret' => $clientSecret,
        "redirect_uri" => $redirectUri,
      ];
    }
    else {
      $message = $this->t('DTuber\YouTubeService: Credentials not present.');
      $this->dtuberLogger->notice($message);
      return FALSE;
    }
  }

  /**
   * Constructor for YouTube service.
   */
  public function __construct(ConfigFactory $config_factory, LoggerChannelFactory $logger_factory, $state) {
    $this->configFactory = $config_factory;
    $this->dtuberLogger = $logger_factory->get('dtuber');
    $this->state = $state;

    try {
      if (!$this->getCredentials()) {
        // Credentials present.
        return FALSE;
      }
      // Set client.
      $this->client = new \Google_Client();

      // Initialize client.
      $this->client->setClientId($this->clientId);
      $this->client->setClientSecret($this->clientSecret);
      $this->client->setScopes('https://www.googleapis.com/auth/youtube');
      $this->client->setRedirectUri($this->redirectUri);
      // These two are required to get refresh_token.
      $this->client->setAccessType("offline");
      $this->client->setApprovalPrompt("force");

    }
    catch (\Exception $e) {
      $message = $this->t('Dtuber Error : @e', ['@e' => $e->getMessage()]);
      $this->dtuberLogger->error($message);
    }
  }

  /**
   * Manages google token.
   */
  public function manageTokens() {
    // Calculate token expiry.
    $token = $this->state->get('dtuber_access_token');
    $this->client->setAccessToken($token);
    // And perform required action.
    if ($this->client->isAccessTokenExpired()) {
      // If Token expired.
      // we need to refresh token in this case.
      // Check whether we have a refresh token or not.
      $refreshToken = $this->state->get('dtuber_refresh_token');
      $this->client->refreshToken($refreshToken);
      $this->updateTokenFromClient();
    }
    else {
      // Good TOken. Continue.
    }
  }

  /**
   * Update tokens.
   */
  protected function updateTokenFromClient() {
    $this->state->set('dtuber_access_token', $this->client->getAccessToken());
    $this->state->set('dtuber_refresh_token', $this->client->getRefreshToken());
  }

  /**
   * Return config from dtuber config settings.
   */
  protected function getConfig($config) {
    return $this->configFactory->get('dtuber.settings')->get($config);
  }

  /**
   * Revoke an OAuth2 access token or refresh token.
   *
   * This method will revoke current access token, if a token isn't provided.
   */
  public function revokeAuth() {
    return $this->client->revokeToken();
  }

  /**
   * Gets AuthURL.
   */
  public function getAuthUrl() {
    return $this->client->createAuthUrl();
  }

  /**
   * Authorizes client.
   */
  public function authorizeClient($code) {
    $this->client->setAccessType("offline");
    $this->client->authenticate($code);

    // Store token into state storage.
    $this->state->set('dtuber_access_token', $this->client->getAccessToken());
    $this->state->set('dtuber_refresh_token', $this->client->getRefreshToken());

  }

  /**
   * Uploads video to YouTube.
   */
  public function uploadVideo($options = NULL) {
    $videoPath = '';
    try {
      // Will set tokens & refresh token when necessary.
      $this->manageTokens();

      $youtube = new \Google_Service_YouTube($this->client);
      if (isset($options)) {
        $videoPath = '.' . urldecode($options['path']);
      }

      // Video category.
      $snippet = new \Google_Service_YouTube_VideoSnippet();
      // Set Title.
      $title = (isset($options)) ? $options['title'] : '';
      $snippet->setTitle($title);
      // Set Description.
      $description = (isset($options)) ? $options['description'] : '';
      $snippet->setDescription($description);
      // Set Tags.
      $tags = (isset($options)) ? $options['tags'] : [];
      $snippet->setTags($tags);

      // Numeric video category. See
      // https://developers.google.com/youtube/v3/docs/videoCategories/list
      $snippet->setCategoryId("22");

      // Set the video's status to "public". Valid statuses are "public",
      // "private" and "unlisted".
      $status = new \Google_Service_YouTube_VideoStatus();
      $privacy_status = $options['privacy_status'] ?? 'public';
      $status->privacyStatus = $privacy_status;

      // Associate the snippet and status objects with a new video resource.
      $video = new \Google_Service_YouTube_Video();
      $video->setSnippet($snippet);
      $video->setStatus($status);

      // Specify the size of each chunk of data, in bytes. Set a higher value
      // for reliable connection as fewer chunks lead to faster uploads.
      // Set a lower value for better recovery on less reliable connections.
      $chunkSizeBytes = 1 * 1024 * 1024;

      // Setting the defer flag to true tells the client to return a request
      // which can be called with ->execute(); instead of making
      // the API call immediately.
      $this->client->setDefer(TRUE);

      // Create a request for the API's videos.insert method to create
      // and upload the video.
      $insertRequest = $youtube->videos->insert("status,snippet", $video);

      // Create a MediaFileUpload object for resumable uploads.
      $media = new \Google_Http_MediaFileUpload(
        $this->client,
        $insertRequest,
        'video/*',
        NULL,
        TRUE,
        $chunkSizeBytes
      );
      $media->setFileSize(filesize($videoPath));

      // Read the media file and upload it chunk by chunk.
      $status = FALSE;
      $handle = fopen($videoPath, "rb");
      while (!$status && !feof($handle)) {
        $chunk = fread($handle, $chunkSizeBytes);
        $status = $media->nextChunk($chunk);
      }

      fclose($handle);

      // If you want to make other calls after the file upload,
      // set setDefer back to false.
      $this->client->setDefer(FALSE);

      $youtubelink = 'http://youtube.com/watch?v=' . $status['id'];
      $message = $this->t('Upload Successful! Video might take a while to process. <a href=":youtube" target="_Blank">Watch in YouTube</a>', [':youtube' => $youtubelink]);
      $rendered_message = Markup::create($message);

      // Set Playlists if provided.
      if (!empty($options['playlist'])) {
        $this->insertVideoToPlaylist($status['id'], $options['playlist']);
      }

      // Returns an array of important values;.
      return [
      // Status of OK means video successfully uploaded.
        'status' => 'OK',
        'message' => $rendered_message,
        'video_id' => $status['id'],
      ];
    }
    catch (\Exception $e) {
      $message = $this->t('Dtuber Error : @e', ['@e' => $e->getMessage()]);
      $this->dtuberLogger->error($message);
    }
    // By default it sends false value.
    return [
    // Status of ERROR means, video not uploaded.
      'status' => 'ERROR',
    ];
  }

  /**
   * Service to retrive YouTube Account Owner details.
   */
  public function youTubeAccount() {
    try {

      // Will set tokens & refresh token when necessary.
      $this->manageTokens();

      $youtube = new \Google_Service_YouTube($this->client);

      $channelsResponse = $youtube->channels->listChannels(
        'brandingSettings', [
          'mine' => 'true',
        ]
      );

      $branding = $channelsResponse->getItems()[0]->getBrandingSettings();
      $channel = (!empty($branding)) ? $branding->getChannel() : NULL;
      if ($channel == NULL) {
        return FALSE;
      }

      return $channel;
    }
    catch (\Exception $e) {
      $message = $this->t('DTuber Error : @e', ['@e' => $e->getMessage()]);
      $this->dtuberLogger->error($message);
    }
  }

  /**
   * Inserts video to playlists.
   */
  protected function insertVideoToPlaylist($videoID, $playlistId) {
    $resourceId = new \Google_Service_YouTube_ResourceId();
    $resourceId->setVideoId($videoID);
    $resourceId->setKind('youtube#video');

    $playlistItemSnippet = new \Google_Service_YouTube_PlaylistItemSnippet();

    $playlistItemSnippet->setPlaylistId($playlistId);
    $playlistItemSnippet->setResourceId($resourceId);

    $playlistItem = new \Google_Service_YouTube_PlaylistItem();
    $playlistItem->setSnippet($playlistItemSnippet);

    $youtubeService = new \Google_Service_YouTube($this->client);
    $youtubeService->playlistItems->insert('snippet,contentDetails', $playlistItem, array());

  }

  /**
   * Returns a list of playlist for the channel.
   */
  public function getPlaylists() {
    // Contains an array of playlists in the form of [id => title].
    $playlist_list = [];
    $this->manageTokens();
    $youtubeService = new \Google_Service_YouTube($this->client);
    $playlists = $youtubeService->playlists;

    $response = $playlists->listPlaylists('snippet', ['mine' => TRUE]);
    $items = $response->items;
    foreach ($items as $item) {
      $playlist_list[$item->id] = $item->snippet->title;
    }
    return $playlist_list;
  }

}
