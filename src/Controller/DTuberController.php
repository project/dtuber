<?php

namespace Drupal\dtuber\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Messenger\MessengerInterface;

/**
 * Dtuber controller for Authorization & Revoking user access.
 */
class DTuberController extends ControllerBase {

  /**
   * The dtuber_youtube_service service.
   *
   * @var \Drupal\dtuber\YouTubeService
   */
  protected $dtuberYtService;

  /**
   * The currently active request object.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\Messenger
   */
  protected $messenger;

  /**
   * The state storage service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * {@inheritdoc}
   */
  public function __construct($dtuberYoutube, $request, MessengerInterface $messenger, $state) {
    $this->dtuberYtService = $dtuberYoutube;
    $this->request = $request;
    $this->messenger = $messenger;
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('dtuber_youtube_service'),
      $container->get('request_stack')->getCurrentRequest(),
      $container->get('messenger'),
      $container->get('state')
    );
  }

  /**
   * Revokes Google Authorization.
   *
   * @param bool $showmsg
   *   (optional) Controls display of authentication revoked message.
   */
  public function revoke($showmsg = TRUE) {
    $this->state->set('dtuber_access_token', NULL);
    $this->dtuberYtService->revokeAuth();

    if ($showmsg) {
      $this->messenger->addMessage($this->t('Authentication Revoked. Need re authorization from Google.'));
    }

    return $this->redirect('dtuber.configform');
  }

  /**
   * Authorizes User.
   */
  public function authorize() {
    // Handles dtuber/authorize authorization from google.
    $code = $this->request->query->get('code');
    $error = $this->request->query->get('error');
    // Authorize current request.
    $this->dtuberYtService->authorizeClient($code);
    $this->messenger->addMessage($this->t('New Token Authorized.'));

    if ($code) {
      if ($this->dtuberYtService->youTubeAccount() === FALSE) {
        $this->messenger->addMessage();
        $this->messenger
          ->addError($this->t('YouTube account not configured properly.'));
        $this->revoke(FALSE);
      }

    }
    elseif ($error == 'access_denied') {
      $this->messenger
        ->addError($this->t('Access Rejected! grant application to use your account.'));
    }
    // Redirect to configform.
    return $this->redirect('dtuber.configform');

  }

}
