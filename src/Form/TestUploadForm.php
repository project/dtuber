<?php

namespace Drupal\dtuber\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Test upload form for Dtuber.
 */
class TestUploadForm extends FormBase {

  /**
   * The dtuber_youtube_service service.
   *
   * @var \Drupal\dtuber\YouTubeService
   */
  protected $dtuberYtService;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\Messenger
   */
  protected $messenger;

  /**
   * The file storage service.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $fileStorage;

  /**
   * The file URL generator.
   *
   * @var \Drupal\Core\File\FileUrlGeneratorInterface
   */
  protected $fileUrlGenerator;

  /**
   * {@inheritdoc}
   */
  public function __construct($dtuberYoutube, MessengerInterface $messenger, EntityStorageInterface $file_storage, FileUrlGeneratorInterface $file_url_generator) {
    $this->dtuberYtService = $dtuberYoutube;
    $this->messenger = $messenger;
    $this->fileStorage = $file_storage;
    $this->fileUrlGenerator = $file_url_generator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('dtuber_youtube_service'),
      $container->get('messenger'),
      $container->get('entity_type.manager')->getStorage('file'),
      $container->get('file_url_generator')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'dtuber_test_upload_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $hasAccessToken = \Drupal::state()->get('dtuber_access_token');
    // If Google authentication not authorised then redirect to configure page.
    if (!$hasAccessToken) {
      $this->messenger()->addError($this->t('Please Authorize first before using the test upload functionality.'));
      return new RedirectResponse(\Drupal\Core\Url::fromRoute('dtuber.configform')->toString());
    }

    $form['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Video Title'),
      '#description' => $this->t('Provide Title for this Video.'),
      '#required' => TRUE,
    ];
    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Video Description'),
    ];
    $allowed_exts = ['mov mp4 avi mkv ogv webm 3gp flv'];
    $exts = implode(', ', explode(' ', $allowed_exts[0]));
    $video_desc = $this->t('Allowed Extensions: :extensions', [':extensions' => $exts]);
    $form['video'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Upload a Video'),
      '#description' => $video_desc,
      '#upload_location' => 'public://dtuber_files',
      '#upload_validators' => [
        'file_validate_extensions' => $allowed_exts,
      ],
      '#required' => TRUE,
    ];
    $form['tags'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Video Tags'),
      '#description' => $this->t('Enter comma separated tags'),
    ];
    $form['playlist'] = [
      '#type' => 'select',
      '#title' => $this->t('Playlist'),
      '#description' => $this->t('Select the playlist you want your video to upload to.'),
      '#options' => [NULL => $this->t('None')] + $this->dtuberYtService->getPlaylists(),
    ];
    $form['privacy_status'] = [
      '#type' => 'select',
      '#title' => $this->t('Video Privacy Status'),
      '#options' => [
        'public' => $this->t('Public'),
        'unlisted' => $this->t('Unlisted'),
        'private' => $this->t('Private'),
      ],
    ];


    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Upload to YouTube'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    global $base_url;

    $file = $form_state->getValue('video');
    $file = $this->fileStorage->load($file[0]);
    $path = $this->fileUrlGenerator->generateAbsoluteString($file->getFileUri());

    $options = [
      'path' => str_replace($base_url, '', $path),
      'title' => $form_state->getValue('title'),
      'description' => $form_state->getValue('description'),
      'tags' => explode(',', $form_state->getValue('tags')),
      'playlist' => $form_state->getValue('playlist'),
      'privacy_status' => $form_state->getValue('privacy_status'),
    ];
    $response = $this->dtuberYtService->uploadVideo($options);
    if ($response['status'] != 'OK') {
      $this->messenger->addError($this->t('Unable to upload Video.'));
    }
    else {
      $this->messenger->addMessage($this->t('Video Uploaded successfully.'));
    }
  }

}
