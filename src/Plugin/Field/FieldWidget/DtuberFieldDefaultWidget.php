<?php

namespace Drupal\dtuber\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Plugin implementation of the 'dtuber_field_default_widget' widget.
 *
 * @FieldWidget(
 *   id = "dtuber_field_default_widget",
 *   label = @Translation("Default Dtuber Widget"),
 *   field_types = {
 *       "dtuber_field"
 *   }
 * )
 */
class DtuberFieldDefaultWidget extends WidgetBase implements ContainerFactoryPluginInterface {

  /**
   * The immutable configuration object.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  private $dtuberSettings;

  /**
   * {@inheritdoc}
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, $dtuberSettings) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->dtuberSettings = $dtuberSettings;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('config.factory')->get('dtuber.settings')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    $item = $items[$delta];

    $element['fid'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Video file'),
      '#description' => $this->t('This video file will be uploaded to YouTube.'),
      '#upload_location' => 'public://dtuber_files',
      '#default_value' => NULL,
      '#upload_validators' => [
        'file_validate_extensions' => !empty($this->dtuberSettings->get('allowed_exts')) ? [$this->dtuberSettings->get('allowed_exts')] : ['mov mp4 avi mkv 3gp'],
      // Pass the maximum file size in bytes
      // 'file_validate_size' => array(MAX_FILE_SIZE*1024*1024),.
      ],
    ];
    $element['fid_revision'] = [
      '#type' => 'hidden',
      '#default_value' => NULL,
    ];

    if (!$item->isEmpty()) {
      // When field is NOT empty. Set a default value for fid.
      $element['fid']['#default_value'] = [$item->get('fid')->getValue()];
      $element['fid_revision']['#default_value'] = [$item->get('fid')->getValue()];
    }

    $element['yt_uploaded'] = [
      '#type'  => 'hidden',
      '#default_value' => ($item->get('yt_uploaded')->getValue()) ? $item->get('yt_uploaded')->getValue() : 0,
    ];

    $element['yt_videoid'] = [
      '#type'  => 'hidden',
      '#default_value' => ($item->get('yt_videoid')->getValue()) ? $item->get('yt_videoid')->getValue() : '',
    ];

    $element += [
      '#type' => 'fieldset',
      '#description' => $this->t('DTuber Field: This video will get upload to YouTube'),
    ];

    // If showing additional fields.
    if ($this->getSetting('additional_fields')) {
      $additional_fields = unserialize($item->get('additional_fields')->getValue() ?? '');
      $element['additional_fields'] = [
        '#type' => 'fieldset',
        '#title' => $this->t('Additional fields'),
        '#description' => $this->t('Additional fields data will be uploaded to YouTube.'),
      ];
      $element['additional_fields']['description'] = [
        '#title' => $this->t('Description'),
        '#type' => 'textarea',
        '#default_value' => $additional_fields['description'] ?? '',
      ];
      $element['additional_fields']['tags'] = [
        '#title' => $this->t('Tags (comma separated)'),
        '#type' => 'textfield',
        '#default_value' => $additional_fields['tags'] ?? '',
      ];
      $element['additional_fields']['privacy_status'] = [
        '#title' => $this->t('Privacy Status'),
        '#type' => 'select',
        '#default_value' => $additional_fields['privacy_status'] ?? 'public',
        '#options' => [
          'public' => $this->t('Public'),
          'unlisted' => $this->t('Unlisted'),
          'private' => $this->t('Private'),
        ]
      ];
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    // Help From: http://stackoverflow.com/questions/38996037/drupal-8-field-plugin-with-field-type-managed-file
    foreach ($values as &$value) {
      if (count($value['fid'])) {
        foreach ($value['fid'] as $fid) {
          $value['fid'] = $fid;
        }
      }
      else {
        $value['fid'] = $value['fid_revision'] !== '' ? $value['fid_revision'] : NULL;
      }

    }

    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'additional_fields' => FALSE,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $summary[] = $this->t('Show additional fields in widget: @additional', [
      '@additional' => $this->getSetting('additional_fields') ? $this->t('Yes') : $this->t('No')
    ]);
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element['additional_fields'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show additional fields.'),
      '#description' => $this->t('When checked, Additional Youtube fields while uploading video will be available.'),
      '#default_value' => $this->getSetting('additional_fields'),
      '#weight' => 0,
    ];
    return $element;
  }

}
